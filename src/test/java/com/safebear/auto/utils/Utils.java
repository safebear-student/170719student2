package com.safebear.auto.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    //Define site URL
    private static final String URL = System.getProperty("url", "http://localhost:8080");
    //Define browser
    private static final String BROWSER = System.getProperty("browser", "webdrivermanagerChrome");
    /*
     * Get the URl variable.
     */
    public static String getUrl() {
        return URL;
    }

    /*
     *Get the driver.
     */
    public static WebDriver getDriver() {

        ChromeOptions options = new ChromeOptions();

        FirefoxOptions ffOptions = new FirefoxOptions();
        ffOptions.addArguments("window-size=100%");

        switch (BROWSER.toLowerCase()) {
            case "chrome" :
                options.addArguments("window-size=100%");
                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
                return new ChromeDriver(options);
            case "webdrivermanagerChrome":
                options.addArguments("window-size=100%");
                WebDriverManager.chromedriver().setup();
                return new ChromeDriver(options);
            case "firefox" :
                options.addArguments("window-size=100%");
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            case "edge" :
                options.addArguments("window-size=100%");
                WebDriverManager.edgedriver().setup();
                return new EdgeDriver();
            case "ie":
                options.addArguments("window-size=100%");
                WebDriverManager.iedriver().setup();
                return new InternetExplorerDriver();
            case "headless_chrome" :
                options.addArguments("headless", "disable-gpu");
                WebDriverManager.chromedriver().setup();
                return new ChromeDriver(options);
            case "grid" :
                DesiredCapabilities cap = new DesiredCapabilities();
                ChromeOptions chromeOptions = new ChromeOptions();
                cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                cap.setBrowserName("chrome");
                return new RemoteWebDriver(cap);
            default :
                options.addArguments("window-size=100%");
                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
                return new ChromeDriver(options);
        }
    }
}
