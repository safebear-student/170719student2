package com.safebear.auto.pages;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class BasePage {
    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }
}
