package com.safebear.auto.tests;

import com.safebear.auto.utils.ScreenShot;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogInTest extends BaseTest {

    private static final String expectedLoginPageTitle = "Login Page";
    private static final String expectedToolsPageTitle = "Toolses Page";

    @Test
    public void loginTest() {
        //Step 1 ACTION: Open the web application in the browser
        driver.get(Utils.getUrl());
        //Step 1 EXPECTED RESULT: Check that we're on the login page
        Assert.assertEquals(basePage.getPageTitle(), expectedLoginPageTitle,
                "The Login page didn't open, or the title has changed");
        //Step 2 ACTION: Enter Username and Password
        loginpage.enterUsername("tester");
        loginpage.enterPassword("letmein");
        //Step 3 ACTION: Click the Login Button
        loginpage.clickLoginButton();
        //Step 3 EXPECTED RESULT: Check that we're on the Tools Page
        Assert.assertEquals(basePage.getPageTitle(), expectedToolsPageTitle,
                    "The Tools page didn't open, or the title has changed");
    }
}
