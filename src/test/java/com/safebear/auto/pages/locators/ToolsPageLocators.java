package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;
@Data
public class ToolsPageLocators {

    private By loginButtonLocator = By.xpath(".//td[preceding-sibling::td='jmeter.apache.org'']");
}
