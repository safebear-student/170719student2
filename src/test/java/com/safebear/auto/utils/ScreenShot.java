package com.safebear.auto.utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class ScreenShot {
    /*
     *Take a screenshot.
     */

    public static String currentDate() {
        String dateString;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy,MM,dd-HH,mm,ss");
        Date date = new Date();
        dateString = formatter.format(date).toString();
        return dateString;
    }

    public static void captureScreenShot(WebDriver driver, String fileName) {
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            copy(scrFile, new File("screenshots/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
