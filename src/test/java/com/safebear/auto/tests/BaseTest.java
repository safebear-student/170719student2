package com.safebear.auto.tests;

import com.safebear.auto.pages.BasePage;
import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.ScreenShot;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public abstract class BaseTest {

    WebDriver driver;
    protected BasePage basePage;
    protected LoginPage loginpage;
    protected ToolsPage toolsPage;
    public static String currentMethod;
    public static final String fileType = ".png";

    @BeforeTest
    public void setUp() {
        driver = Utils.getDriver();
        basePage = new BasePage(driver);
        loginpage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @AfterTest
    public void tearDown() throws InterruptedException {

        Thread.sleep(2000);
        ScreenShot.captureScreenShot(driver,  ScreenShot.currentDate() + fileType);
        driver.quit();
    }
}
